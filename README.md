## Setting up: ##
1. install NodeJS
2. cd to this directory
3. run "npm install"

## Running the tool: ##

```
#!shell
    node app.js -h <jira-url> -u <username> -p <password> -P <project_key> -s <number_of_issues> [-t <template_name>]
```

where:

* jira-url = URL of the JIRA instance to populate (e.g. https://jmwe-test-mq.atlassian.net or http://localhost:2990/jira)
* username = the username of the user who will be the reporter of the issues. Must have the appropriate rights
* password = password of aforementioned user
* project\_key = JIRA project in which to create issues
* number\_of\_issues = the number of issues to create
* template_name [optional] = name (with extension) of the template file used to create the issue (see template.json for an example)