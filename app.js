var request = require('request');
var path = require('path');
var os = require('os');
var q = require('q');
var _ = require('lodash');
const commandLineArgs = require('command-line-args');
var nunjucks = require('nunjucks');

const optionDefinitions = [
    {name: 'jira-host', alias: 'h', type: String},
    {name: 'username', alias: 'u', type: String},
    {name: 'password', alias: 'p', type: String},
    {name: 'project', alias: 'P', type: String},
    {name: 'template', alias: 't', type: String, defaultValue: 'template.json'},
    {name: 'size', alias: 's', type: Number}
];

var options = commandLineArgs(optionDefinitions);
var log = console;

var counter = 1;

createOneIssue();

function createOneIssue() {
    var body = JSON.parse(nunjucks.render(options.template, {
        'counter': counter,
        'projectKey': options.project
    }));

    var opts = {
        uri: (options['jira-host'].startsWith('http') ? options['jira-host'] : ('https://' + options['jira-host'] + '.atlassian.net'))+'/rest/api/2/issue',
        auth: {username: options.username, password: options.password},
        method: 'POST',
        body: body,
        json: true
    };

    request(opts, function (error, response, body) {
        if (error) {
            console.log(error);
            process.exit();
        }
        if (response.body && response.body.errors) {
            console.log("JIRA returned an error: " + JSON.stringify(response.body.errors));
            process.exit();
        }
        if (++counter <= options.size)
            createOneIssue();
        else {
            console.log("Created " + options.size + " issues");
            process.exit();
        }
    });
}
